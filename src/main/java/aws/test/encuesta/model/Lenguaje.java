package aws.test.encuesta.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lenguaje")
public class Lenguaje {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLenguaje;
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;
	
	public int getIdLenguaje() {
		return idLenguaje;
	}
	public void setIdLenguaje(int idLenguaje) {
		this.idLenguaje = idLenguaje;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
