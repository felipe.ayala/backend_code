package aws.test.encuesta.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profesion")
public class Profesion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProfesion;
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;
	
	public int getIdProfesion() {
		return idProfesion;
	}
	public void setIdProfesion(int idProfesion) {
		this.idProfesion = idProfesion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
