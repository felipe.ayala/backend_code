package aws.test.encuesta.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne; 

@Entity
@Table(name = "encuesta")
public class Encuesta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEncuesta; 
	@Column(name = "nombres", nullable = false, length = 50)
	private String nombres;
	@Column(name = "apellidos", nullable = false, length = 50)
	private String apellidos;
	@Column(name = "edad", nullable = false)
	private int edad;
	@Column(name = "lugartrabajo", nullable = false, length = 50)
	private String lugartrabajo; 
	@ManyToOne
	@JoinColumn(name = "id_profesion", nullable = false)
	private Profesion profesion;
	
	@ManyToOne
	@JoinColumn(name = "id_lenguaje", nullable = false)
	private Lenguaje lenguaje;
	
	public Lenguaje getLenguaje() {
		return lenguaje;
	}

	public void setLenguaje(Lenguaje lenguaje) {
		this.lenguaje = lenguaje;
	}

	public int getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(int idEncuesta) {
		this.idEncuesta = idEncuesta;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getLugartrabajo() {
		return lugartrabajo;
	}

	public void setLugartrabajo(String lugartrabajo) {
		this.lugartrabajo = lugartrabajo;
	}

	public Profesion getProfesion() {
		return profesion;
	}

	public void setProfesion(Profesion profesion) {
		this.profesion = profesion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idEncuesta;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Encuesta other = (Encuesta) obj;
		if (idEncuesta != other.idEncuesta)
			return false;
		return true;
	}
}
