package aws.test.encuesta.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.test.encuesta.model.Lenguaje;
import aws.test.encuesta.service.ILenguajeService;

@RestController
@RequestMapping("api/lenguaje")
public class ApiLenguajeController {

	@Autowired
	private ILenguajeService service;

	@GetMapping(produces = "application/json")
	public List<Lenguaje> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Lenguaje listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Lenguaje registrar(@RequestBody Lenguaje Lenguaje) {
		return service.registrar(Lenguaje);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Lenguaje modificar(@RequestBody Lenguaje Lenguaje) {
		return service.modificar(Lenguaje);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
