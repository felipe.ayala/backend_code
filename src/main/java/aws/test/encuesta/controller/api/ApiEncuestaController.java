package aws.test.encuesta.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.test.encuesta.model.Encuesta;
import aws.test.encuesta.service.IEncuestaService;

@RestController
@RequestMapping("api/encuesta")
public class ApiEncuestaController {

	@Autowired
	private IEncuestaService service;

	@GetMapping(produces = "application/json")
	public List<Encuesta> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Encuesta listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Encuesta registrar(@RequestBody Encuesta Encuesta) {
		return service.registrar(Encuesta);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Encuesta modificar(@RequestBody Encuesta Encuesta) {
		return service.modificar(Encuesta);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
