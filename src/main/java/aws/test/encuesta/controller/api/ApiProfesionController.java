package aws.test.encuesta.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.test.encuesta.model.Profesion;
import aws.test.encuesta.service.IProfesionService;

@RestController
@RequestMapping("api/profesion")
public class ApiProfesionController {

	@Autowired
	private IProfesionService service;

	@GetMapping(produces = "application/json")
	public List<Profesion> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Profesion listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Profesion registrar(@RequestBody Profesion Profesion) {
		return service.registrar(Profesion);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Profesion modificar(@RequestBody Profesion Profesion) {
		return service.modificar(Profesion);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
