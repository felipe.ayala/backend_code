package aws.test.encuesta.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import aws.test.encuesta.model.Lenguaje; 

public interface ILenguajeDAO extends JpaRepository<Lenguaje, Integer> {
}
