package aws.test.encuesta.dao;

import aws.test.encuesta.model.Profesion;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IProfesionDAO extends JpaRepository<Profesion, Integer> {

} 
