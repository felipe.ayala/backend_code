package aws.test.encuesta.dao;

import aws.test.encuesta.model.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IPersonaDAO extends JpaRepository<Encuesta, Integer> {

} 
