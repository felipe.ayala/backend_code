package aws.test.encuesta.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import aws.test.encuesta.dao.IEncuestaDAO;
import aws.test.encuesta.model.Encuesta;
import aws.test.encuesta.service.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService{

	@Autowired
	private IEncuestaDAO dao;
	
	@Override
	public Encuesta registrar(Encuesta t) {	
		return dao.save(t);
	}

	@Override
	public Encuesta modificar(Encuesta t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Encuesta listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Encuesta> listar() {
		return dao.findAll();
	}
	@Override
	public Page<Encuesta> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}

}
