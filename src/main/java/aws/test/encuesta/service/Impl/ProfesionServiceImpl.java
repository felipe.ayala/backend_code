package aws.test.encuesta.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aws.test.encuesta.dao.IProfesionDAO;
import aws.test.encuesta.model.Profesion;
import aws.test.encuesta.service.IProfesionService;

@Service
public class ProfesionServiceImpl implements IProfesionService{

	@Autowired
	private IProfesionDAO dao;
	
	@Override
	public Profesion registrar(Profesion t) {	
		return dao.save(t);
	}

	@Override
	public Profesion modificar(Profesion t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Profesion listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Profesion> listar() {
		return dao.findAll();
	}

}
