package aws.test.encuesta.service.Impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import aws.test.encuesta.dto.RenewPasswordFirstDTO;
import aws.test.encuesta.dto.RespuestaApi;
import aws.test.encuesta.dto.UpdatePasswordDTO;
import aws.test.encuesta.service.SecurityService;

@Service
public class SecurityServiceImpl implements SecurityService {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public RespuestaApi getToken(String username, String password) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> authParams = new HashMap<String, String>();
		authParams.put("USERNAME", username);
		authParams.put("PASSWORD", password);

		try {
			rpta.setStatus("OK");
			rpta.setIdToken("TOKEN-PRUEBA");
			rpta.setRefreshToken("TOKEN-PRUEBA");
		} catch(Exception e) {
			rpta.setBody("Reinicie su password");
			rpta.setStatus("OK-RESET");
		}

		return rpta;
	}

	@Override
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> challengeResponses = new HashMap<String, String>();
		challengeResponses.put("USERNAME", updatePassword.getUsername());
		challengeResponses.put("NEW_PASSWORD", updatePassword.getPassword());

		try {

		} catch (Exception e) {
			rpta.setBody("Usuario no autorizado");
		}

		return rpta;
	}
	
	@Override
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		try {

			/**
			 * Usar este codigo para cambiar el password siempre y cuando el usuario este en sesion (con token)
			 */
			/*
			ChangePasswordRequest changePasswordRequest= new ChangePasswordRequest()
		              .withAccessToken(updatePassword.getToken())
		              .withPreviousPassword(updatePassword.getOldPassword())
		              .withProposedPassword(updatePassword.getNewPassword());
		 
		    cognitoClient.changePassword(changePasswordRequest);
		    */
		    
		    rpta.setStatus("OK");
			rpta.setBody("Clave cambiada correctamente");
		} catch(Exception e) {
			logger.error("[updatePassword] Ocurrio un error inesperado: ", e);
			rpta.setBody("Ocurrio un error inespera     do");
		}

		return rpta;
	}

	/**
	 * Se puede ingresar el accessToken o idToken
	 */
	@Override
	public RespuestaApi signOut(String token) {
		logger.error(token);
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");
		
		try {
			rpta.setStatus("OK");
			rpta.setBody("SignOut correcto");
		}catch(Exception e) {
			logger.error("[signOut] Ocurrio un error inesperado: ", e);
			rpta.setBody(e.getMessage());
		}
		
		return rpta;
	}

	/**
	 * Se necesita el refreshToken como input
	 */
	@Override
	public RespuestaApi refreshToken(String token) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> authParams = new HashMap<String, String>();
		authParams.put("REFRESH_TOKEN", token);

		try {
			rpta.setStatus("OK");
			rpta.setBody("");
		} catch(Exception e) {
			rpta.setBody("Reinicie su password");
			rpta.setStatus("OK-RESET");
		}

		return rpta;
	}
}
