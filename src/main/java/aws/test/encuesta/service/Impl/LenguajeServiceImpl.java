package aws.test.encuesta.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aws.test.encuesta.dao.ILenguajeDAO;
import aws.test.encuesta.model.Lenguaje;
import aws.test.encuesta.service.ILenguajeService;

@Service
public class LenguajeServiceImpl implements ILenguajeService{

	@Autowired
	private ILenguajeDAO dao;
	
	@Override
	public Lenguaje registrar(Lenguaje t) {	
		return dao.save(t);
	}

	@Override
	public Lenguaje modificar(Lenguaje t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Lenguaje listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Lenguaje> listar() {
		return dao.findAll();
	}

}
