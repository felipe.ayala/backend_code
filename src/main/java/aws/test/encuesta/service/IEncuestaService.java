package aws.test.encuesta.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import aws.test.encuesta.model.Encuesta;

public interface IEncuestaService extends ICRUD<Encuesta>{
	Page<Encuesta> listarPageable(Pageable pageable);
}
